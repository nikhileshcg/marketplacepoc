<?php

namespace Bistro\ProgressiveWebApp\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            /**
             * Create new table bistro_push_subscription
             */
            $installer = $setup;
            $installer->startSetup();

            $table = $installer->getConnection()->newTable(
                            $installer->getTable('bistro_push_subscription')
                    )->addColumn(
                            'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 10, ['identity' => true, 'nullable' => false, 'primary' => true], 'Subscription Id'
                    )->addColumn(
                            'entity_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true], 'Customer Id'
                    )->addColumn(
                            'customer_email', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 30, [], 'Customer Email'
                    )->addColumn(
                            'customer_group_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 10, ['unsigned' => true], 'customer_group_id'
                    )->addColumn(
                            'store_code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 30, [], 'store_code'
                    )->addColumn(
                            'token', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 250, [], 'Push token'
                    )->addColumn(
                            'device', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 30, [], 'Device name'
                    )->addColumn(
                            'subscription_status_code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 30, [], 'Subscription Status Code'
                    )->addColumn(
                            'is_active', \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN, null, ['unsigned' => true, 'default' => 1], 'Token Status'
                    )->addColumn(
                            'creation_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Creation Time'
                    )->addColumn(
                            'update_time', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Update Time'
                    )->addIndex(
                            $installer->getIdxName(
                                    'bistro_push_subscription', ['entity_id'], \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                            ), ['entity_id'], ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
                    )->addForeignKey(
                            $installer->getFkName('bistro_push_notification_customer', 'entity_id', 'customer_entity', 'entity_id'), 'entity_id', $installer->getTable('customer_entity'), 'entity_id', \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                    )->setComment(
                    'Save customer token'
            );
            $installer->getConnection()->createTable($table);
            $installer->endSetup();
       
            $installer = $setup;
            $installer->startSetup();
            $installer->getConnection()->addColumn(
                $installer->getTable('bistro_pwa_notification'), 
                'identifier', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 50,
                'nullable' => true,
                'comment' => 'Notification Identifier',
                'after' => 'notification_id'
                ]
            );
            $installer->endSetup();
      
            $installer->getConnection()->addIndex(
                    $installer->getTable('bistro_pwa_notification'),
                    'BISTRO_PWA_NOTIFICATION_IDENTIFIER', ['identifier'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE);
        }

        $installer->endSetup();
    }

}
