<?php
/**
 * ProgressiveWebApp
 * 
 * @category    Bistro
 * @package     Bistro_ProgressiveWebApp
 * @author      Faisal Shaikh <faisal.a.shaikh@capgemini.com>
 * @copyright   Copyright (c) 2018 Bistro
 */

namespace Bistro\ProgressiveWebApp\Helper;

use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use Bistro\ProgressiveWebApp\Model\Subscription;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_storeManager;
    protected $_scope;
    protected $_client;
    protected $_message;
    protected $_notification;
    protected $_subscription;
    protected $_logger;
    protected $_onlineCustomerCollectionFactory;


    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scope,
        Client $client,
        Message $message,
        Subscription $subscriber,
        \Bistro\ProgressiveWebApp\Model\Notification $notification,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Customer\Model\ResourceModel\Online\Grid\CollectionFactory $onlineCustomerCollectionFactory
    )
    {
        $this->_storeManager = $storeManager;
        $this->_client = $client;
        $this->_message = $message;
        $this->_notificationFactory = $notification;
        $this->_subscription = $subscriber;
        $this->_logger = $logger;
        $this->_onlineCustomerCollectionFactory = $onlineCustomerCollectionFactory;
        parent::__construct($context);
    }

    public function getStore()
    {
        return $this->_storeManager->getStore();
    }

    public function isEnabled()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/general/enable',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestShortName()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/short_name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestName()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestDescription()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/description',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestStartUrl()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/start_url',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestThemeColor()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/theme_color',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestBgColor()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/background_color',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestDisplayType()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/display_type',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestOrientation()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/orientation',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestIcon()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/icon',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getManifestIconSizes()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/manifest/icon_sizes',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getFirebaseScript()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/notification/fcm_config',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getServerKey()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/notification/fcm_server_key',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getTopicName()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/notification/fcm_topic_name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getMessageSenderId()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/notification/fcm_messaging_sender_id',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function getDeleteTokeApiUrl()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/notification/fcm_delete_url_config',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

     public function getCacheVersionName()
    {
        return $this->scopeConfig->getValue(
            'progressivewebapp/notification/cache_version_name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()->getId()
        );
    }

    public function logger()
    {
        return $this->_logger;
    }
    /**
     * Send Push Notification
     *
     * @param Array $template Template Data
     * @param Integer $customerId Customer Id
     */
    public function sendNotification($template, $customerId)
    {
        if (!empty($customerId) && !empty($template)) {
            $notificationData = $template;
            $icon = unserialize($notificationData['icon']);
            $server_key = $this->getServerKey();
            $senderKey = $this->getTokenByCustomerId($customerId);

            if (!empty($senderKey)) {
                $this->_client->setApiKey($server_key);
                $this->_client->injectGuzzleHttpClient(new \GuzzleHttp\Client());
                $this->_notification = new Notification($notificationData['title'], $notificationData['body']);
                $this->_message->addRecipient(new Device($senderKey));
                $this->_message->setNotification($this->_notification);
                $this->_notification->setIcon($icon[0]['url']);
                $this->_notification->setClickAction($notificationData['target_url']);
                $onlineCustomers = $this->_onlineCustomerCollectionFactory->create();
                $onlinecustomers = $onlineCustomers->load();
                $onlinecustomers = $onlinecustomers->getData();
                foreach($onlinecustomers as $onlinecustomer){
                  $onlinecustomerId[]=$onlinecustomer['customer_id'];
                }
                if (in_array($customerId,$onlinecustomerId)){
                try {
                    $send = $this->_client->send($this->_message);
                    $reponse = $send->getStatusCode();
                    if ($reponse == 200) {
                        $this->_logger->debug('Send notification successfully.');
                    } else {
                        $this->_logger->debug('Send notification failed. Please try again.');
                    }
                } catch (\Exception $e) {
                    $this->_logger->debug($e->getMessage());
                    $this->_logger->debug('Something went wrong while sending notification');
                }
              }
            }
        }
    }

    public function getTokenByCustomerId($customerId) {
        $key = '';
        $tokenCollection = $this->_subscription->getCollection()
                ->addFieldToSelect('token')
                ->addFieldToFilter('entity_id', $customerId)
                ->getData();
        if (!empty($tokenCollection)) {
            $key = $tokenCollection[0]['token'];
        }
        return $key;
    }

    public function getTemplateIdByName($param) {
        $id = '';
        $collection = $this->_notificationFactory->getCollection()
                ->addFieldToSelect('notification_id')
                ->addFieldToFilter('identifier', $param)
                ->getData();
        if (!empty($collection)) {
            foreach ($collection as $value) {
                $id = $value['notification_id'];
            }
        }
        return $id;
    }

}
