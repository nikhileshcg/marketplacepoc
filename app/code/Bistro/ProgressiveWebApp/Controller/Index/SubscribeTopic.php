<?php

namespace Bistro\ProgressiveWebApp\Controller\Index;

use Bistro\ProgressiveWebApp\Model\Subscription;

class SubscribeTopic extends \Magento\Framework\App\Action\Action {

    protected $resultJsonFactory;
    protected $scope;
    protected $_curl;
    protected $_pwaHelper;
    protected $_logger;
    protected $_storeManagerInterface;


    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Framework\App\Config\ScopeConfigInterface $scope, \Magento\Framework\HTTP\Client\Curl $curl, \Bistro\ProgressiveWebApp\Helper\Data $pwaHelper, \Psr\Log\LoggerInterface $logger, \Magento\Store\Model\StoreManagerInterface $store
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->scope = $scope;
        $this->_curl = $curl;
        $this->_pwaHelper = $pwaHelper;
        $this->_logger = $logger;
        $this->_storeManagerInterface = $store;
        return parent::__construct($context);
    }

    public function execute() {
        $result = $this->resultJsonFactory->create();
        $data = $this->getRequest()->getPost();
        $url = $data['subUrl'];
        $params = [];
        $topicName = $this->_pwaHelper->getTopicName();
        $serverKey = $this->_pwaHelper->getServerKey();
        $subscribeUrl = $url . '/rel/topics/' . $topicName;
        try {
            $this->_curl->addHeader('Content-Type', 'application/json');
            $this->_curl->addHeader('Authorization', 'key=' . $serverKey);
            $this->_curl->post($subscribeUrl, $params);
            $this->saveToken($data);
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
        return $result->setData($this->_curl->getStatus());
    }

    public function saveToken($data) {
        try {
            $subscriptionModel = $this->getSubscription();
            $customer = $this->getCustomerData($data['customerId'])->getData();
            $token = $this->checkIfTokenExist($data);
            if (!empty($token['status'])) {
                $subscriptionModel->load($token['token_id'])
                        ->setCustomerGroupId($customer['group_id'])
                        ->setCustomerEmail($customer['email'])
                        ->setStoreCode($this->_storeManagerInterface->getStore()->getCode())
                        ->setToken($data['token'])
                        ->setSubscriptionStatusCode($this->_curl->getStatus())
                        ->setIsActive(1)
                        ->save();
            } else {
                $subscriptionModel->setEntityId($data['customerId'])
                        ->setCustomerGroupId($customer['group_id'])
                        ->setCustomerEmail($customer['email'])->setStoreCode($this->_storeManagerInterface->getStore()->getCode())
                        ->setToken($data['token'])
                        ->setSubscriptionStatusCode($this->_curl->getStatus())
                        ->setIsActive(1)
                        ->save();
            }
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }

    /**
     * Check if customer token already exist
     * @param array $data
     * @return array
     */
    public function checkIfTokenExist($data) {
        $response = array();
        $response['status'] = false;
        $response['token_id'] = '';
        $collection = $this->getSubscription()->getCollection()->getData();
        foreach ($collection as $value) {
            if ($data['customerId'] == $value['entity_id']) {
                $response['status'] = true;
                $response['token_id'] = $value['id'];
            }
        }
        return $response;
    }

    /**
     * Return Subscription data
     * 
     * @return mixed
     */
    protected function getSubscription() {
        return $this->_objectManager->create('\Bistro\ProgressiveWebApp\Model\Subscription');
    }

    protected function getCustomerData($customerId) {
        if (!empty($customerId)) {
            return $this->_objectManager->create('\Magento\Customer\Model\Customer')->load($customerId);
        }
    }

}
