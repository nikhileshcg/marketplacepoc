<?php

namespace Bistro\ProgressiveWebApp\Observer;

use Braintree\Exception;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class UpdateServiceWorker implements ObserverInterface
{
    protected $_pwaHelper;
    protected $urlBuilder;
    protected $directoryList;
    private $assetRepo;
    protected $file;

    public function __construct(
        \Bistro\ProgressiveWebApp\Helper\Data $pwaHelper,
        UrlInterface $urlBuilder,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\Filesystem\Io\File $file
    )
    {
        $this->_pwaHelper = $pwaHelper;
        $this->urlBuilder = $urlBuilder;
        $this->directoryList = $directoryList;
        $this->assetRepo = $assetRepo;
        $this->file = $file;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $randomNumber = rand(10, 100);
        $cacheVersionName = $this->_pwaHelper->getCacheVersionName();
        if ($this->_pwaHelper->isEnabled() && $this->_pwaHelper->isEnabled() == '1') {
            $data = "
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

firebase.initializeApp({
    'messagingSenderId': '295744071712'
});

const messaging = firebase.messaging();
var CACHE_NAME = \"$cacheVersionName\";
var SWversion = \"$randomNumber\";

self.addEventListener('install', function (event) {
    // Perform install steps
    event.waitUntil(
        self.skipWaiting()
    );
});

self.addEventListener('activate', function (event) {
    console.log('[ServiceWorker] Activate');
    event.waitUntil(
        caches.keys().then(function (keys) {
            return Promise.all(keys.map(function (key, i) {
                console.log('Old CACHE', key);
                   if (key !== CACHE_NAME) {    
                        console.log('[ServiceWorker] Removing old cache', key);
                          return caches.delete(key);
                        }
                    }));
        }));
    return self.clients.claim()
});



self.addEventListener('fetch', function (event) {
    if (event.request.method !== 'POST' && event.request.url.toString() &&
        event.request.url.toString().indexOf('/admin/') === -1 &&
        event.request.url.toString().indexOf('/checkout/') === -1 &&
        event.request.url.toString().indexOf('/cart/index/') === -1 &&
        event.request.url.toString().indexOf('/key/') === -1 &&
        event.request.url.toString().indexOf('/seller/') === -1 &&
        event.request.url.toString().indexOf('/customer/account/') === -1 &&
        event.request.url.toString().indexOf('/sales/order/') === -1 &&
        event.request.url.toString().indexOf('/sales/order/view/') === -1 &&
        event.request.url.toString().indexOf('/wishlist/') === -1 &&
        event.request.url.toString().indexOf('/customer/section/load/') === -1 &&
        event.request.url.toString().indexOf('/search/parts/result/') === -1 &&
        event.request.url.toString().indexOf('/adminhtml/') === -1) {
        event.respondWith(
            caches.match(event.request)
                .then(function (response) {
                    // Cache hit - return response
                    if (response) {
                        return response;
                    }

                    // IMPORTANT: Clone the request. A request is a stream and
                    // can only be consumed once. Since we are consuming this
                    // once by cache and once by the browser for fetch, we need
                    // to clone the response.
                    var fetchRequest = event.request.clone();

                    return fetch(fetchRequest).then(
                        function (response) {
                            // Check if we received a valid response
                            if (!response || response.status !== 200 || response.type !== 'basic') {
                                return response;
                            }

                            // IMPORTANT: Clone the response. A response is a stream
                            // and because we want the browser to consume the response
                            // as well as the cache consuming the response, we need
                            // to clone it so we have two streams.

                            var responseToCache = response.clone();
                            caches.open(CACHE_NAME)
                                .then(function (cache) {
                                    cache.put(event.request, responseToCache);

                                });

                            return response;
                        }
                    );
                })
        );
    }
});

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[serviceWorker.js] Received background message ', payload);
});

self.addEventListener('notificationclick', function (event) {
    event.notification.close();
});";
            $ioAdapter = $this->file;
            $rootPath = $this->directoryList->getPath(DirectoryList::ROOT) . \DIRECTORY_SEPARATOR;

            try {
                if (file_exists($rootPath . 'serviceWorker.js')) {
                    $ioAdapter->rm($rootPath . 'serviceWorker.js');
                }
                file_put_contents($rootPath . 'serviceWorker.js', $data);

            } catch (\Exception $exception) {
                $this->_pwaHelper->logger()->error($exception);
            }
        }
    }

}
