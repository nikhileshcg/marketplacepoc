<?php

namespace Bistro\ProgressiveWebApp\Block\Base\Js;

use Magento\Framework\View\Element\Template;

class Main extends \Magento\Framework\View\Element\Template
{
    protected $_helper;
    
    protected $_customerSession;

    public function __construct(
        Template\Context $context,
        \Bistro\ProgressiveWebApp\Helper\Data $helper,
        \Magento\Customer\Model\Session $session,
        array $data = []
    )
    {
        $this->_helper = $helper;
        $this->_customerSession = $session;
        parent::__construct($context, $data);
    }

    public function getFirebaseScript()
    {
        return $this->_helper->getFirebaseScript();
    }

    public function getThemeColor()
    {
        return $this->_helper->getManifestThemeColor();
    }
    
    public function getCustomerId() {
        $customerId = 0;
        if ($this->_customerSession->isLoggedIn()) {
            $customerId = $this->_customerSession->getCustomerId();
        }
        return $customerId;
    }

}
