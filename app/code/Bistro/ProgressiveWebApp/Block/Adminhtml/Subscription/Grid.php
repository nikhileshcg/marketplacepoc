<?php

namespace Bistro\ProgressiveWebApp\Block\Adminhtml\Subscription;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Bistro\ProgressiveWebApp\Model\subscriptionFactory
     */
    protected $_subscriptionFactory;

    /**
     * @var \Bistro\ProgressiveWebApp\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Bistro\ProgressiveWebApp\Model\subscriptionFactory $subscriptionFactory
     * @param \Bistro\ProgressiveWebApp\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, \Bistro\ProgressiveWebApp\Model\SubscriptionFactory $SubscriptionFactory, \Bistro\ProgressiveWebApp\Model\Status $status, \Magento\Framework\Module\Manager $moduleManager, array $data = []
    ) {
        $this->_subscriptionFactory = $SubscriptionFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection() {
        $collection = $this->_subscriptionFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns() {
        $this->addColumn(
                'id', [
            'header' => __('ID'),
            'type' => 'number',
            'index' => 'id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
                ]
        );



        $this->addColumn(
                'entity_id', [
            'header' => __('Customer Id'),
            'index' => 'entity_id',
                ]
        );

        $this->addColumn(
                'customer_email', [
            'header' => __('Email'),
            'index' => 'customer_email',
                ]
        );

        $this->addColumn(
                'customer_group_id', [
            'header' => __('Customer Group'),
            'index' => 'customer_group_id',
                ]
        );

        $this->addColumn(
                'store_code', [
            'header' => __('Store'),
            'index' => 'store_code',
                ]
        );

        $this->addColumn(
                'token', [
            'header' => __('Device Token'),
            'index' => 'token',
                ]
        );

        $this->addColumn(
                'device', [
            'header' => __('Device Type'),
            'index' => 'device',
                ]
        );

        $this->addColumn(
                'subscription_status_code', [
            'header' => __('Subcription Status'),
            'index' => 'subscription_status_code',
                ]
        );
        
        $this->addColumn(
                'is_active', [
            'header' => __('Is Active'),
            'index' => 'is_active',
                ]
        );
        
        $this->addColumn(
                'creation_time', [
            'header' => __('Creation Time'),
            'index' => 'creation_time',
            'type'  => 'datetime'
                ]
        );
        
        $this->addColumn(
                'update_time', [
            'header' => __('Updation Time'),
            'index' => 'update_time',
            'type'  => 'datetime'
                ]
        );
        //$this->addColumn(
        //'edit',
        //[
        //'header' => __('Edit'),
        //'type' => 'action',
        //'getter' => 'getId',
        //'actions' => [
        //[
        //'caption' => __('Edit'),
        //'url' => [
        //'base' => '*/*/edit'
        //],
        //'field' => 'id'
        //]
        //],
        //'filter' => false,
        //'sortable' => false,
        //'index' => 'stores',
        //'header_css_class' => 'col-action',
        //'column_css_class' => 'col-action'
        //]
        //);



        $this->addExportType($this->getUrl('progressivewebapp/*/exportCsv', ['_current' => true]), __('CSV'));
        $this->addExportType($this->getUrl('progressivewebapp/*/exportExcel', ['_current' => true]), __('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction() {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('Bistro_ProgressiveWebApp::subscription/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('subscription');

        $this->getMassactionBlock()->addItem(
                'delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('progressivewebapp/*/massDelete'),
            'confirm' => __('Are you sure?')
                ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
                'status', [
            'label' => __('Change status'),
            'url' => $this->getUrl('progressivewebapp/*/massStatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
                ]
        );


        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('progressivewebapp/*/index', ['_current' => true]);
    }

    /**
     * @param \Bistro\ProgressiveWebApp\Model\subscription|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row) {

        return $this->getUrl(
                        'progressivewebapp/*/edit', ['id' => $row->getId()]
        );
    }

}
