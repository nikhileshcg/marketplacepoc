<?php
namespace Bistro\ProgressiveWebApp\Block\Adminhtml\Subscription\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('subscription_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Subscription Information'));
    }
}