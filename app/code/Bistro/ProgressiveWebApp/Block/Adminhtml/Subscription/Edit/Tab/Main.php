<?php

namespace Bistro\ProgressiveWebApp\Block\Adminhtml\Subscription\Edit\Tab;

/**
 * Subscription edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Bistro\ProgressiveWebApp\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Bistro\ProgressiveWebApp\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Bistro\ProgressiveWebApp\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('subscription');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

		
        $fieldset->addField(
            'entity_id',
            'text',
            [
                'name' => 'entity_id',
                'label' => __('Customer Id'),
                'title' => __('Customer Id'),
				'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'customer_email',
            'text',
            [
                'name' => 'customer_email',
                'label' => __('Email'),
                'title' => __('Email'),
				
                'disabled' => $isElementDisabled
            ]
        );
					
        $fieldset->addField(
            'customer_group_id',
            'text',
            [
                'name' => 'customer_group_id',
                'label' => __('Customer Group'),
                'title' => __('Customer Group'),
				
                'disabled' => $isElementDisabled
            ]
        );
		
        
        $fieldset->addField(
            'token',
            'text',
            [
                'name' => 'token',
                'label' => __('Device Token'),
                'title' => __('Device Token'),
				
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'subscription_status_code',
            'text',
            [
                'name' => 'subscription_status_code',
                'label' => __('Status Code'),
                'title' => __('Status Code'),
				
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'is_active',
            'text',
            [
                'name' => 'is_active',
                'label' => __('Device Type'),
                'title' => __('Device Type'),
				
                'disabled' => $isElementDisabled
            ]
        );
        

        $fieldset->addField(
            'device',
            'text',
            [
                'name' => 'device',
                'label' => __('Device Type'),
                'title' => __('Device Type'),
				
                'disabled' => $isElementDisabled
            ]
        );


        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);
		
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    public function getTargetOptionArray(){
    	return array(
    				'_self' => "Self",
					'_blank' => "New Page",
    				);
    }
}
