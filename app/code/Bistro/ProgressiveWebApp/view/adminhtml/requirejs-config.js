

var config = {
    paths: {
        'Bistro/colorpicker': 'jquery/colorpicker/js/colorpicker'
    },
    shim: {
        'Bistro/colorpicker': {
            deps: ['jquery']
        }
    }
};
