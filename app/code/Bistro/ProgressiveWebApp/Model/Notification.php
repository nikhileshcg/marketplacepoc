<?php

namespace Bistro\ProgressiveWebApp\Model;

class Notification extends \Magento\Framework\Model\AbstractModel
{

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'pwa_notification';
    const SALES_ORDER_PLACED = 'new_order_confirm_message';
    const SALES_ORDER_ACK = 'new_order_acknowledgement_message';
    const SALES_ORDER_INVOICE = 'new_order_invoice_message';
    const SALES_CONFIRM_ORDER_SELLER = 'new_order_seller_confirm_message';
    const SALES_ORDER_DISPATCH = 'new_order_dispatched_message';
    const SALES_ORDER_CANCEL = 'new_order_cancel_message';
    const SALES_ORDER_CANCEL_BUYER = 'new_order_buyer_cancel_message';
    const SALES_ORDER_REFUND = 'new_order_refund_intiate_message';
    const SALES_ORDER_REFUND_BUYER = 'new_order_refund_seller_message';
    const SALES_ORDER_BUYER_RATING = 'new_order_buyer_rating';
    const SALES_ORDER_RATING_SELLER_MESSAGE ='new_order_rating_seller_message';

    protected $_cacheTag = 'pwa_notification';

    /**
     * Prefix of model name
     *
     * @var string
     */
    protected $_notificationPrefix = 'pwa_notification';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init('Bistro\ProgressiveWebApp\Model\ResourceModel\Notification');
    }
}
