<?php

namespace Bistro\ProgressiveWebApp\Model\Notification;

use Bistro\ProgressiveWebApp\Model\ResourceModel\Notification\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $_loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $contactCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $notificationCollectionFactory,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $notificationCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $notification) {
            $notificationData = $notification->getData();
            $iconData = unserialize($notification['icon']);
            if (!empty($iconData)) {
                $notificationData['icon'] = array();
                $notificationData['icon'][0]['name'] = $iconData[0]['name'];
                $notificationData['icon'][0]['url'] = $iconData[0]['url'];
//                $notificationData['icon'][0]['path'] = $iconData[0]['path'];
                $notificationData['icon'][0]['file'] = $iconData[0]['file'];
            }
            $this->_loadedData[$notification->getId()]['notification'] = $notificationData;
        }
        return $this->_loadedData;
    }
}
