require(['jquery', 'slick'], function($){
    $(function(){
               $(".parent-slick").slick({
                infinite: false,
                dots: true,
                arrows: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1
            });
    });


    $(".parent-slick-multi").slick({
        infinite: false,
        dots: false,
        arrows: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 2,
        variableWidth: true
    });
    
    $(".parent-slick-partner").slick({
        infinite: false,
        dots: false,
        arrows: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3
    });
    
    
    $(".parent-slick-review").slick({
        infinite: false,
        dots: false,
        arrows: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    });

     $('.ask-quote-btn').click(function(){
        $('.quote-content .quote').toggleClass('show');
        $('.ask-quote-btn').toggleClass('grey-color');
    });


         $("#variant1").click(function(){
            $("#variant-choose").text("Choose your variant");
          });
          $("#variant2").click(function(){
            $("#variant-choose").text("Choose your color");
          });
          $("#variant3").click(function(){
            $("#variant-choose").text("Choose your Accessories");
          });
          $("#variant4").click(function(){
            $("#variant-choose").text("Choose your Extended Waranty");
          });
          $("#variant5").click(function(){
            $("#variant-choose").text("Choose your insurance");
          });
         $("#variant6").click(function(){
            $("#variant-choose").text("Choose your loan");
          });
  

     $("#variant-button1").click(function(){
        $(this).text("Selected").css("background-color", "yellow");
        $("#variant-button2").text("Select").css("background-color", "white");
        $("#variant-button3").text("Select").css("background-color", "white");
       
      });


    $("#variant-button2").click(function(){
        $(this).text("Selected").css("background-color", "yellow");
         $("#variant-button1").text("Select").css("background-color", "white");
        $("#variant-button3").text("Select").css("background-color", "white");
       
      });

    $("#variant-button3").click(function(){
        $(this).text("Selected").css("background-color", "yellow");
         $("#variant-button2").text("Select").css("background-color", "white");
        $("#variant-button1").text("Select").css("background-color", "white");
       
      });


     $("#save").mouseenter(function(){
        $(this).css("background-color", "yellow");
       
      });
     $("#save").mouseleave(function(){
        $(this).css("background-color", "white");
       
      });


     $("#share").mouseenter(function(){
        $(this).css("background-color", "yellow");
       
      });


     $("#share").mouseleave(function(){
        $(this).css("background-color", "white");
       
      });



});