<?php
 
namespace Knowband\Marketplace\Controller\Ajax;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class CarVariant extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
 
    /**
     * @var $_logger
     */
    protected $_logger;

    /**
     * @var Session
     */
    protected $session; 

    protected $_searchVariant;
    
    
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        \Knowband\Marketplace\Block\Search $searchVariant
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_searchVariant = $searchVariant; 
        parent::__construct($context);
        
        
    }
    
    /**
     * Get sub category by category id
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    { 

        $carVariant = array();
        $carFuelType = array();
        $variantOutput = '<option value=""> Variant</option>';
        $fuelTypeOutput = '<option value=""> Fuel Type</option>';
        $productId = $this->getRequest()->getParam('productId');
        
        $productAttribute  = $this->_searchVariant->getProductInfo($productId);

        $carVariant = $productAttribute->getAttributeText('car_variant');
        $carFuelType = $productAttribute->getAttributeText('car_fuel_type');
        $productUrl = $productAttribute->getProductUrl();
        if(is_array($carVariant)){
        foreach($carVariant as $carVariantOpt){
            $variantOutput .= "<option value=".$carVariantOpt.">".$carVariantOpt."</option>";
        } } else{
            $variantOutput .= "<option value=".$carVariant.">".$carVariant."</option>";
        }

        if(is_array($carFuelType)){
        foreach($carFuelType as $carFuelTypeOpt){
            $fuelTypeOutput .= "<option value=".$carFuelTypeOpt.">".$carFuelTypeOpt."</option>";
        }  } else {
            $fuelTypeOutput .= "<option value=".$carFuelType.">".$carFuelType."</option>";  
        }

        $result = $this->resultJsonFactory->create();
        $resultPage = $this->resultPageFactory->create();
        return $result->setData([
                                    'status' => "sucess" . $productId, 
                                    'productUrl' => $productUrl,
                                    'variantOutput' => $variantOutput,
                                    'fuelTypeOutput' => $fuelTypeOutput
                                ]);
     
    }
}