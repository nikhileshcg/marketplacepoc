<?php
 
namespace Knowband\Marketplace\Controller\Index;
 
use Magento\Framework\App\Action\Context;
 
class Test extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    protected $_searchVariant;
 
    public function __construct(Context $context, 
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Knowband\Marketplace\Block\Search $searchVariant
        )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_searchVariant = $searchVariant; 
        parent::__construct($context);
    }
 
    public function execute()
    {
        $carVariant = array();
        $carFuelType = array();
        $variantOutput = '<option value=""> Variant</option>';
        $fuelTypeOutput = '<option value=""> Fuel Type</option>';
       // $productId = $this->getRequest()->getParam('productId');
        $productId = '22';
        $productAttribute  = $this->_searchVariant->getProductInfo($productId);

        $carVariant = $productAttribute->getAttributeText('car_variant');
        $carFuelType = $productAttribute->getAttributeText('car_fuel_type');

        if(is_array($carVariant)){
        foreach($carVariant as $carVariantOpt){
            $variantOutput .= "<option value=".$carVariantOpt.">".$carVariantOpt."</option>";
        } } else{
            $variantOutput .= "<option value=".$carVariant.">".$carVariant."</option>";
        }

        if(is_array($carFuelType)){
        foreach($carFuelType as $carFuelTypeOpt){
            $fuelTypeOutput .= "<option value=".$carFuelTypeOpt.">".$carFuelTypeOpt."</option>";
        }  } else {
            $fuelTypeOutput .= "<option value=".$carFuelType.">".$carFuelType."</option>";  
        }

        echo "<pre>"; print_r($fuelTypeOutput); exit;   

        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
    }


}