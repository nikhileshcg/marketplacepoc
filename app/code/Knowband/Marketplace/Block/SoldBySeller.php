<?php

namespace Knowband\Marketplace\Block;

class SoldBySeller extends \Magento\Framework\View\Element\Template {

    public $product_id = 0;
    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Knowband\Marketplace\Model\Product $mpProductSellerModel,
            \Magento\Framework\Pricing\Helper\Data $priceHelper,
            \Magento\Framework\Registry $registry,
            \Knowband\Marketplace\Helper\Data $helperData
    ) {
        $this->mp_productToSellerModel = $mpProductSellerModel;
        $this->_storeManager = $context->getStoreManager();
        $this->_registry = $registry;
        parent::__construct($context);
        $this->product_id =  $this->getRequest()->getParam('id');
        $this->_priceHelper  =$priceHelper;
        $this->_helperData = $helperData;
        
    }

    public function getSoldBySellerList() {
        $storeId = $this->_storeManager->getStore()->getId();

        $seller_info = $this->mp_productToSellerModel->getCollection();
//		$seller_info->addFieldToFilter('main_table.store_id', array('eq' => $store_id));
        $seller_info->addFieldToFilter('main_table.product_id', ['eq' => $this->product_id]);

        $seller_info->getSelect()->join(['e1' => $seller_info->getTable('vss_mp_seller_entity')], 'e1.seller_id=main_table.seller_id');
        
         $seller_data = $seller_info->getData();
        unset($seller_info);

        if (!empty($seller_data)) {
//            $sellerData = $seller_data[0];
            return $seller_data;
        } else {
            return false;
        }
    }
    
    public function getCurrentProduct()
    {        
        return $this->_registry->registry('current_product');
    }   
    
    
    public function getSettingByKey($seller_id, $key){
        return $this->mp_settingsHelper->getSettingByKey($seller_id, $key);
    }
    
    public function formattedPrice($price){
        return $this->_priceHelper->currency($price, true, false);
    }
    
    public function calculateDiscountPrice($sellingPrice, $origionalPrice){
        $decreaseValue = $origionalPrice - $sellingPrice;
        $discount =( ($decreaseValue/$origionalPrice) * 100);
      //  return "(Save ".(number_format($discount, 2. "%)";
        
    }
    
    public function ratingImagePath(){
        return $this->_helperData->getCustomerMediaPath().'rating_icon.png';
    }
    
    
}
