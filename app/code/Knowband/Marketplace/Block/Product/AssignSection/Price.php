<?php

namespace Knowband\Marketplace\Block\Product\AssignSection;

class Price extends \Knowband\Marketplace\Block\Product\Base 
{    
    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setsFactory,
            \Magento\Framework\ObjectManagerInterface $objectManager,
            \Magento\Framework\Registry $registry,
            \Knowband\Marketplace\Model\Product $productToSellerModel,
            \Magento\Customer\Model\Session $customerSessionModel
    ) {
        $this->_coreRegistry = $registry;
        $this->_setsFactory = $setsFactory;
        $this->_objectManager = $objectManager;
        $this->mp_productToSellerModel = $productToSellerModel;
        $this->_customerSessionModel = $customerSessionModel;
        
        parent::__construct($context, $setsFactory, $objectManager, $registry);
        $this->setTemplate('product/assignsection/price.phtml');
    }
    
     public function getCurrentSellerProduct($productId){
        $product_to_seller_coll = [];
        $customer = $this->_customerSessionModel->getCustomer()->getData();
         $product_to_seller_coll = $this->mp_productToSellerModel->getCollection()
                    ->addFieldToFilter("seller_id", ["eq" => $customer['entity_id']])
                    ->addFieldToFilter("product_id", ["eq" => $productId])->getFirstItem();
        return $product_to_seller_coll;
    }
    
}
