<?php

namespace Knowband\Marketplace\Block\Product\Compare;
//Knowband\Marketplace\Block\Product\Compare\SellerPrice
class SellerPrice extends \Magento\Framework\View\Element\Template { 
    
    public $product_id = 0;
    
    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Framework\Registry $registry,
            \Knowband\Marketplace\Helper\Setting $mpSettingHelper, 
            \Knowband\Marketplace\Helper\Data $mpDataHelper, 
            \Knowband\Marketplace\Model\Product $mpProductSellerModel
    ) {
        $this->mp_settingsHelper = $mpSettingHelper;
        $this->mp_dataHelper = $mpDataHelper;
        $this->mp_productToSellerModel = $mpProductSellerModel;
        $this->_coreRegistry = $registry;
        $this->_storeManager = $context->getStoreManager();
        parent::__construct($context);
        $this->product_id =  $this->getRequest()->getParam('id');
    }
    
}
