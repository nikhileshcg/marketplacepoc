<?php

namespace Knowband\Marketplace\Block;

use Magento\Customer\Model\Session;
use Magento\Directory\Model\CurrencyFactory;

class Search extends \Magento\Framework\View\Element\Template {


    protected $session;

    protected $_storeManager;

    protected $mpSellerModel;

    private $currencyCode;

    protected $_productCollectionFactory;
  
    protected $_productVisibility;


    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Framework\ObjectManagerInterface $objectManager,
            \Knowband\Marketplace\Model\Product $mpProductToSellerModel,
            \Knowband\Marketplace\Helper\Setting $mpSettingHelper,
            \Knowband\Marketplace\Helper\Data $mpDataHelper,
            \Knowband\Marketplace\Helper\Reports $mpReportsHelper,
            \Knowband\Marketplace\Helper\Log $mpLogHelper,
            \Magento\Catalog\Model\ProductFactory $_productloader,
             \Magento\Store\Model\StoreManagerInterface $storeManager,
             \Knowband\Marketplace\Model\Seller $mpSellerModel,
             Session $customerSession,
             CurrencyFactory $currencyFactory,
             \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $productVisibility
    ) {
        $this->_objectManager = $objectManager;
        $this->mp_productToSellerModel = $mpProductToSellerModel;
        $this->mp_settingHelper = $mpSettingHelper;
        $this->mp_dataHelper = $mpDataHelper;
        $this->mp_reportsHelper = $mpReportsHelper;
        $this->_productloader = $_productloader;
        $this->mp_logHelper = $mpLogHelper;
        $this->session = $customerSession;
        $this->_storeManager = $storeManager;
        $this->mp_SellerModel = $mpSellerModel;
        $this->currencyCode = $currencyFactory->create();
        $this->_productCollectionFactory = $productCollectionFactory; 
        $this->_productVisibility = $productVisibility; 
        parent::__construct($context);
    }
    
    


    public function getProductCollections() {
       
        try {

            $collection = $this->_productCollectionFactory->create();
            $collection->addAttributeToSelect('*')
            ->addAttributeToSort('entity_id', 'desc')
            ->addAttributeToFilter('status', '1');
        } catch (\Exception $ex) {
                $this->mp_logHelper->createFileAndWriteLogData(
                        \Knowband\Marketplace\Helper\Log::INFOTYPEERROR, 'Helper Product::getSellerEnabledProducts()', $ex->getMessage()
                );
            }

        return $collection;

    }




     public function getAllSellerEnabledProducts() {
        $sellerProducts = [];
        $website_id = 1;

       // echo "hi"; exit();
        try {

            $productCollection = $this->mp_productToSellerModel->getCollection()
                    ->addFieldToFilter('website_id', (int) $website_id)
                    ->addFieldToFilter('approved', 1)
                    ->addFieldToSelect('product_id')->distinct(true);


            $products = $productCollection->getData();

          //  echo "<pre>Hi"; print_r($productCollection->getData()); exit;

            unset($productCollection);

            foreach ($products as $pro) {
                $sellerProducts[] = $pro['product_id'];
            }

            if (empty($sellerProducts)) {
                return [0];
            }

        } catch (\Exception $ex) {
            $this->mp_logHelper->createFileAndWriteLogData(
                    \Knowband\Marketplace\Helper\Log::INFOTYPEERROR, 'Helper Product::getSellerEnabledProducts()', $ex->getMessage()
            );
        }

        return $sellerProducts;
    }


    public function getProductInfo($produtId)
    {
        return $this->_productloader->create()->load($produtId);
    }

    public function getPostActionUrl()
    {
        return $this->getUrl('marketplace/sellers/result');
    }

    public function getImageName($produtId)
    {
        $productImg = $this->_productloader->create()->load($produtId);
      //  echo "<pre>"; print_r($productImg->getPrice()); exit;
        return $this->getStoreBaseUrl().'pub/media/catalog/product/'.$productImg->getData('image');
    }

     public function getStoreBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }

    public function getSearchSellerList()
    {

        $postParams = $this->session->getSearchPostParams();
        try {
                if(array_key_exists('model_number', $postParams)){
                    if(!empty($postParams['model_number'])){

                        $productCollection = $this->mp_productToSellerModel->getCollection()
                            ->addFieldToFilter('product_id', $postParams['model_number'])
                            ->addFieldToSelect('seller_id')
                            ->addFieldToSelect('product_id')
                            ->addFieldToSelect('price')
                            ->setOrder('price', 'asc');

                        $sellerIds = $productCollection->getData();
                        unset($productCollection);

                    }
                }
        } catch (\Exception $ex) {
            $this->mp_logHelper->createFileAndWriteLogData(
                    \Knowband\Marketplace\Helper\Log::INFOTYPEERROR, 'Helper Product::getSellerEnabledProducts()', $ex->getMessage()
            );
        }

        return $sellerIds;

       // echo "<pre>"; print_r($sellerIds); exit;
    }

    public function getSellerDetails($sellerId)
    {

        $sellerCollection = $this->mp_SellerModel->getCollection()
                            ->addFieldToFilter('seller_id', $sellerId);
        $sellerDetail = $sellerCollection->getData();

            unset($sellerCollection);

            $sellerInfo = array();
            foreach ($sellerDetail as $sellerDetails) {
                $sellerInfo = $sellerDetails;
            }
        // echo "<pre>"; print_r($sellerInfo['shop_title']); exit();
         return $sellerInfo;

    }

    public function getCurrencySymbol(){

        $currentCurrency = $this->_storeManager->getStore()->getCurrentCurrencyCode();
        $currency = $this->currencyCode->load($currentCurrency);
        return $currency->getCurrencySymbol();
    }



}
