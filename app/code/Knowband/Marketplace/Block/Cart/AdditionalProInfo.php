<?php

namespace Knowband\Marketplace\Block\Cart;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Block\Cart\Additional\Info as AdditionalBlockInfo;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template as ViewTemplate;
use Magento\Framework\View\Element\Template\Context;


class AdditionalProInfo extends ViewTemplate {

/**
     * Product
     *
     * @var ProductInterface|null
     */
    protected $product = null;

    /**
     * Product Factory
     *
     * @var ProductInterfaceFactory
     */
    protected $productFactory;
    /**
     * AdditionalBlockInfo
     *
     * @var AdditionalBlockInfo
     */
    protected $_additionalBlockInfo;

    /**
     * CartItemBrandBlock constructor
     *
     * @param Context $context
     * @param ProductInterfaceFactory $productFactory
     */
    public function __construct(
        Context $context,
        \Psr\log\loggerInterface $logger,
            AdditionalBlockInfo $additionalBlockInfo,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_additionalBlockInfo =$additionalBlockInfo;
        $this->_logger = $logger;
    }
    
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
    }
    
    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }
     /**
     * Get Product Brand Text
     *
     * @return string
     */
    public function getAdditionalData()
    {
        $this->_logger->critical(print_r($this->_additionalBlockInfo->getItem()));
        
        return "sample Vendor";
    }
   
}
