<?php

namespace Knowband\Marketplace\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;



class checkoutCartProductAddAfter implements ObserverInterface {

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
            \Magento\Customer\Model\Session $session,
            \Psr\Log\LoggerInterface $logger,
            \Magento\Framework\Event\Manager $eventManager,
            \Magento\Framework\ObjectManagerInterface $objectManager,
            RequestInterface $request,
            \Knowband\Marketplace\Model\Product $mpProductSellerModel
    ) {
        $this->_session = $session;
        $this->_logger = $logger;
        $this->_eventManager = $eventManager;
        $this->_objectManager = $objectManager;
        $this->_request = $request;
        $this->_mpProductSellerModel =$mpProductSellerModel;
    }

    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        try {
            
//            $customer = $observer->getEvent()->getCustomer();
//            $customer_data = $customer->getData();
//            $is_seller = $this->mp_sellerHelper->isSeller($customer_data['entity_id']);
            
            $isCustomerLoggedIn = $this->_session->isLoggedIn();
            
            if ($isCustomerLoggedIn) {
             // type logged in custome code
            }
            $requestParams = $this->_request->getParams();
            if(isset($requestParams['vendor-checked-id'])){
                     $seller_info = $this->_mpProductSellerModel->getCollection();
                        $seller_info->addFieldToFilter('product_id', ['eq' => $requestParams['item']]);
                        $seller_info->addFieldToFilter('seller_id', ['eq' => $requestParams['vendor-checked-id']]);
                        $seller_data = $seller_info->getData();
                        unset($seller_info);
                        if (!empty($seller_data)) {
                                $sellerData = $seller_data[0];
                               $price = $sellerData['price'];
                               $item = $observer->getEvent()->getData('quote_item');
                                $item = ( $item->getParentItem() ? $item->getParentItem() : $item );
                                $item->setCustomPrice($price);
                                    $item->setVendorId($requestParams['vendor-checked-id']);
                                $item->setOriginalCustomPrice($price);
                                $item->getProduct()->setIsSuperMode(true);
                            } 
                        
            }
            $this->_logger->critical(print_r($_SESSION, true));
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }
    }
}
